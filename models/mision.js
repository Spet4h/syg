import mongoose,{Schema} from 'mongoose'

const MisionSchema = new Schema({
  description: String,
  state:{type:Boolean,default:true}, 
  createAt: {type:Date,default:Date.now()}  
},{versionKey:false})

const Mision = mongoose.model('Mision',MisionSchema);

export default Mision;