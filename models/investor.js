import mongoose, { Schema } from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'
const rolesValidos = {
   values: ['INVERSIONISTA'],
   message: '{VALUE} is not a valid role' 
}

const InvestorSchema = new Schema({
  name: {type:String,required:['name is required',true]},
  email: {type:String, required:['email is required',true],unique:true},
  phone:{type:String,default:'NOT FOUND'},
  password:{type:String,required:true},
  userName: {type:String, required:true},
  role:{type:String,enum:rolesValidos},
  state:{type:Boolean,default:true},
  createAt:{type:Date,default:Date.now()}  
},{versionKey:false})

// Ocultar la password en la response
InvestorSchema.methods.toJSON = function(){
  const { password, _id,...user} = this.toObject();
  user.uid = _id;
  return user; 
}

InvestorSchema.plugin(uniqueValidator,{message:'{PATH} must be unique'});
const Investor = mongoose.model('Investor',InvestorSchema);

export default Investor;
