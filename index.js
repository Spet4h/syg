// HEROKU CONFIG
// require('./config/config')
// LOCAL CONFIG
require('dotenv').config()
import express from 'express'
import cors from 'cors'
import mongoose from 'mongoose'
import morgan from 'morgan'
import path from 'path'
import router from './routes'
const app = express();

app.use(morgan('tiny'));
app.use(cors());
app.use(express.json());

//application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

//MIDDLEWARES ROUTES
app.use(router);
app.use(express.static('public'));
const history = require('connect-history-api-fallback');
app.use(history());
app.use(express.static(path.join(__dirname, 'public')));

// connection database local
mongoose.connect(process.env.URL_DB,{useNewUrlParser:true},(err)=>{

// connection database heroku
// mongoose.connect(process.env.MONGO_URI,{useNewUrlParser:true},(err)=>{
       
  if(err){
    console.log(`Error connectiong to the database ${err}`)  
  }else{
    console.log('Connection to the established database');   
  }
  app.listen(process.env.PORT,()=>{
    console.log(`Listen on port ${process.env.PORT}`)  
  })
})