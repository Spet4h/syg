import { response, request } from 'express';
import jwt from 'jsonwebtoken';
import Entrepreneur from '../models/entrepreneur';

import Investor from '../models/investor'

export default {
  validarJWT: async(req,res,next)=>{
    // Leer token de los headers
    const token = req.header('x-token');
    
    if(!token){
      // 401 no authorization  
      return res.status(401).json({
        msg:'No hay token en la petición'  
      })  
    }
    try {
      // Verificar si el token en válido
      const { uid } = jwt.verify(token,process.env.SECRETORPRIVATEKEY);
      
      // Leer el usuario que corresponde al uid
      const entrepreneur  = await Entrepreneur.findById(uid);
      if(!entrepreneur){
        return res.status(401).json({
          msg: 'Token no válido - emprendedor no exist DB '  
        })  
      }

      // Verificar si el uid tiene estado true
      if(!entrepreneur.state){
          return res.status(401).json({
            msg:'Token no válido - usuario con estado:false'  
          }) 
      }
    
      req.entrepreneur = entrepreneur;
      next();
    } catch (error) {
      console.log(`An error ocurred ${error}`);
      res.status(401).json({
        msg:'Token no válido'  
      }) 
    }
  },
  validarJWTI: async(req,res,next)=>{
    // Leer token de los headers
    const token = req.header('x-token');
    
    if(!token){
      // 401 no authorization  
      return res.status(401).json({
        msg:'No hay token en la petición'  
      })  
    }
    try {
      // Verificar si el token en válido
      const { uid } = jwt.verify(token,process.env.SECRETORPRIVATEKEY);
      
      // Leer el usuario que corresponde al uid
      const investor  = await Investor.findById(uid);
      if(!investor){
        return res.status(401).json({
          msg: 'Token no válido - inversor no exist DB '  
        })  
      }

      // Verificar si el uid tiene estado true
      if(!investor.state){
          return res.status(401).json({
            msg:'Token no válido - usuario con estado:false'  
          }) 
      }
    
      req.investor = investor;
      next();
    } catch (error) {
      console.log(`An error ocurred ${error}`);
      res.status(401).json({
        msg:'Token no válido'  
      }) 
    }
  },

}

// ORIGINAL
// const validarJWT = async(req= request,res=response,next)=>{
//   // Leer token de los headers
//   const token = req.header('x-token');
  
//   if(!token){
//     // 401 no authorization  
//     return res.status(401).json({
//       msg:'No hay token en la petición'  
//     })  
//   }
//   try {
//     // Verificar si el token en válido
//     const { uid } = jwt.verify(token,process.env.SECRETORPRIVATEKEY);
    
//     // Leer el usuario que corresponde al uid
//     const entrepreneur  = await Entrepreneur.findById(uid);
//     if(!entrepreneur){
//       return res.status(401).json({
//         msg: 'Token no válido - emprendedor no exist DB '  
//       })  
//     }

//     // Verificar si el uid tiene estado true
//     if(!entrepreneur.state){
//         return res.status(401).json({
//           msg:'Token no válido - usuario con estado:false'  
//         }) 
//     }
  
//     req.entrepreneur = entrepreneur;
//     next();  
//   } catch (error) {
//     console.log(error);
//     res.status(401).json({
//       msg:'Token no válido'  
//     })  
//   }
// }
// export default validarJWT; 