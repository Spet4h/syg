import express from 'express'
import projectController from '../controllers/projectController'

// Validate role
import role from '../middlewares/validar-roles'
// validate JWT
import validateJWT from '../middlewares/validar-jwt'
const router = express.Router();

router.get('/projects',
   // validateJWT.validarJWTI,
   // role.roleInversionista,
   // Emprendedor
//    validateJWT.validarJWT,
//    role.roleEmprendedor,
   projectController.getProjects
)
// router.post('/project',validateJWT,projectController.addProject)
// router.delete('/project/:id',validateJWT,projectController.deleteProject)

export default router

