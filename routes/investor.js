import express from 'express'
import investorController from '../controllers/investorController'
import login from '../controllers/auth'
// Validate role
import role from '../middlewares/validar-roles'
// validate JWT
import validateJWT from '../middlewares/validar-jwt'

const router = express.Router()

router.get('/investors',/* validateJWT,role.roleEmprendedor, */investorController.getInvestors);
router.get('/investor/:id',/* validateJWT,role.roleEmprendedor, */investorController.getInvestor);
router.post('/investor',/* validateJWT, */investorController.addInvestor);
router.put('/investor/:id',/* validateJWT, */investorController.updateInvestor);
router.delete('/investor/:id',/* validateJWT, */investorController.deleteInvestor);
router.post('/login-investor',login.loginInvestor);

export default router; 
