import express from 'express'
import visionController from '../controllers/visionController'
import role from '../middlewares/validar-roles'
import validateJWT from '../middlewares/validar-jwt'

const router = express.Router();

router.get('/vision',/* validateJWT, role.roleEmprendedor,*/visionController.getVision);
router.post('/vision',visionController.addVision);
router.put('/vision/:id',visionController.updateVision);

export default router;