import express from 'express'
import aboutController from '../controllers/aboutController'

// Validate role
import role from '../middlewares/validar-roles'
// validate JWT
import validateJWT from '../middlewares/validar-jwt'

const router = express.Router();

router.get('/about',/* validateJWT, role.roleEmprendedor,*/aboutController.getAbouts);
router.post('/about',/* validateJWT, */aboutController.addAbout);

export default router;