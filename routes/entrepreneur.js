import express from 'express'
import entrepreneurController from '../controllers/entrepreneurController'
import login from '../controllers/auth'
// Validate role
import role from '../middlewares/validar-roles'
// validate JWT
import validateJWT from '../middlewares/validar-jwt'

const router = express.Router()

/*=================== GETS ORIGINAL ===================*/
// router.get('/entrepreneurs',validateJWT.validarJWTI,role.roleInversionista,entrepreneurController.getEntrepreneurs);
router.get('/entrepreneurs',/* validateJWT.validarJWTI,role.roleInversionista, */entrepreneurController.getEntrepreneurs);
router.get('/entrepreneurs',validateJWT.validarJWTI,role.roleInversionista,entrepreneurController.getEntrepreneurs);
router.get('/entrepreneur/:id',entrepreneurController.getEntrepreneur);
router.post('/entrepreneur',/* validateJWT, */entrepreneurController.addEntrepreneur);
router.put('/entrepreneur/:id',/* validateJWT, */entrepreneurController.updateEntrepreneur);
router.delete('/entrepreneur/:id',/* validateJWT, */entrepreneurController.deleteEntrepreneur);
router.post('/login-entrepreneur',login.login);

export default router; 
