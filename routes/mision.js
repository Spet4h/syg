import express from 'express'
import misionController from '../controllers/misionController'

// Validate role
import role from '../middlewares/validar-roles'
// validate JWT
import validateJWT from '../middlewares/validar-jwt'

const router = express.Router();

router.get('/mision',/* validateJWT, role.roleEmprendedor,*/misionController.getMision);
router.post('/mision',/* validateJWT, */misionController.addMision);
router.put('/mision/:id',/* validateJWT, */role.roleEmprendedor,misionController.updateMision);

export default router;