import Entrepreneur from '../models/entrepreneur'
import bcrypt from 'bcryptjs'

export default {
    addEntrepreneur: async(req,res,next)=>{
      try {
        const body = req.body;
        body.password = await bcrypt.hashSync(body.password,10);
        const entrepreneur = await Entrepreneur.create(body);
        res.status(200).json(entrepreneur);  
      } catch (err) {
        res.status(500).send({
           message: `An error ocurred ${err}`
        }) 
        next(err);
      }
    },
     getEntrepreneurs: async(req,res,next)=>{
       try {
           const entrepreneurs = await Entrepreneur.find({})
            // .populate('project')
           res.status(200).json(entrepreneurs);  
         } catch (err) {
           res.status(500).send({
              message: `An error ocurred ${err}`
           }) 
           next(err);
       }
     },
     getEntrepreneur: async(req,res,next)=>{
       try {
          let {id } = req.params;
           const entrepreneur = await Entrepreneur.findById(id)
           res.status(200).json(entrepreneur);  
         } catch (err) {
           res.status(500).send({
              message: `An error ocurred ${err}`
           }) 
           next(err);
       }
     },
     updateEntrepreneur: async(req,res,next)=>{
       try {
           const { id } = req.params;
           const update  = req.body;
           // Encriptar password
        //    body.password = await bcrypt.hashSync(body.password,10);
           let pass = req.body.password;
           const reg = await Entrepreneur.findById(id);
           // NO ME DEJA ACTUALIZAR LOS DEMAS CAMPOS.
           if(!req.body.password){
             console.log('NO ESTA ACTUALIZANDO LA CONTRASEÑA');
           }else{
              if(pass!= reg.password){
                req.body.password = await bcrypt.hashSync(req.body.password,10);
              }
             
           }
           const entrepreneur  = await Entrepreneur.findByIdAndUpdate(id,update,{new:true});
           res.status(200).json(entrepreneur);
         } catch (err) {
           res.status(500).send({
              message: `An error ocurred ${err}`
           }) 
           next(err);
       }
     },
     deleteEntrepreneur: async(req,res,next)=>{
       try {
           const { id } = req.params;
           const update  = req.body;
           //Change state user
          //  const user  = await User.findByIdAndUpdate(id,{state:false},{new:true});
           // Delete user
           const entrepreneur = await Entrepreneur.findByIdAndDelete(id);
   
           res.status(200).json({message:'Entrepreneur delete succesfully'});
         } catch (err) {
           res.status(500).send({
              message: `An error ocurred ${err}`
           }) 
           next(err);
       } 
     }  
}