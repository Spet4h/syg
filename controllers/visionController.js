import Vision from '../models/vision'

export default {
 addVision: async(req,res,next)=>{
   try {
     const body = req.body;
     const vision = await Vision.create(body);
     res.status(200).json(vision);  
   } catch (err) {
     res.status(500).send({
        message: `An error ocurred ${err}`
     }) 
     next(err);
   }
 },
  getVision: async(req,res,next)=>{
    try {
        const vision = await Vision.find({})
        res.status(200).json(vision);  
      } catch (err) {
        res.status(500).send({
           message: `An error ocurred ${err}`
        }) 
        next(err);
    }
  },
  updateVision: async(req,res,next)=>{
    try {
        const { id } = req.params;
        const update  = req.body;
        const vision  = await Vision.findByIdAndUpdate(id,update,{new:true});
        res.status(200).json(vision);
      } catch (err) {
        res.status(500).send({
           message: `An error ocurred ${err}`
        }) 
        next(err);
    }
  },
  deleteVision: async(req,res,next)=>{
    try {
        const { id } = req.params;
        const update  = req.body;
        const mision  = await Vision.findByIdAndUpdate(id,{state:false},{new:true});

        res.status(200).json({message:'Vision delete succesfully'});
      } catch (err) {
        res.status(500).send({
           message: `An error ocurred ${err}`
        }) 
        next(err);
    } 
  }  
}