import Investor from '../models/investor'
import bcrypt from 'bcryptjs'

export default {
    addInvestor: async(req,res,next)=>{
      try {
        const body = req.body;
        body.password = await bcrypt.hashSync(body.password,10);
        const investor = await Investor.create(body);
        res.status(200).json(investor);  
      } catch (err) {
        res.status(500).send({
           message: `An error ocurred ${err}`
        }) 
        next(err);
      }
    },
     getInvestors: async(req,res,next)=>{
       try {
           const investors = await Investor.find({})
            // .populate('project')
           res.status(200).json(investors);  
         } catch (err) {
           res.status(500).send({
              message: `An error ocurred ${err}`
           }) 
           next(err);
       }
     },
     // ById
     getInvestor: async(req,res,next)=>{
       try {
           let {id} = req.params;
           const investorID = await Investor.findById(id);
           res.status(200).json(investorID);  
         } catch (err) {
           res.status(500).send({
              message: `An error ocurred ${err}`
           }) 
           next(err);
       }
     },
     updateInvestor: async(req,res,next)=>{
       try {
           const { id } = req.params;
           const update  = req.body;
           // Encriptar password
        //    body.password = await bcrypt.hashSync(body.password,10);
           let pass = req.body.password;
           const reg = await Investor.findById(id);
           // NO ME DEJA ACTUALIZAR LOS DEMAS CAMPOS.
           if(!req.body.password){
             console.log('NO ESTA ACTUALIZANDO LA CONTRASEÑA');
           }else{
              if(pass!= reg.password){
                req.body.password = await bcrypt.hashSync(req.body.password,10);
              }
             
           }
           const investor  = await Investor.findByIdAndUpdate(id,update,{new:true});
           res.status(200).json(investor);
         } catch (err) {
           res.status(500).send({
              message: `An error ocurred ${err}`
           }) 
           next(err);
       }
     },
     deleteInvestor: async(req,res,next)=>{
       try {
           const { id } = req.params;
           const update  = req.body;
           //Change state user
          //  const user  = await User.findByIdAndUpdate(id,{state:false},{new:true});
           // Delete user
           const investor = await Investor.findByIdAndDelete(id);
   
           res.status(200).json({message:'Investor delete succesfully'});
         } catch (err) {
           res.status(500).send({
              message: `An error ocurred ${err}`
           }) 
           next(err);
       } 
     }  
}