import Mision from '../models/mision'

export default {
 addMision: async(req,res,next)=>{
   try {
     const body = req.body;
     const mision = await Mision.create(body);
     res.status(200).json(mision);  
   } catch (err) {
     res.status(500).send({
        message: `An error ocurred ${err}`
     }) 
     next(err);
   }
 },
  getMision: async(req,res,next)=>{
    try {
        const mision = await Mision.find({})
        res.status(200).json(mision);  
      } catch (err) {
        res.status(500).send({
           message: `An error ocurred ${err}`
        }) 
        next(err);
    }
  },
  updateMision: async(req,res,next)=>{
    try {
        const { id } = req.params;
        const update  = req.body;
        const mision  = await Mision.findByIdAndUpdate(id,update,{new:true});
        res.status(200).json(mision);
      } catch (err) {
        res.status(500).send({
           message: `An error ocurred ${err}`
        }) 
        next(err);
    }
  },
  deleteMision: async(req,res,next)=>{
    try {
        const { id } = req.params;
        const update  = req.body;
        const mision  = await Mision.findByIdAndUpdate(id,{state:false},{new:true});

        res.status(200).json({message:'Mision delete succesfully'});
      } catch (err) {
        res.status(500).send({
           message: `An error ocurred ${err}`
        }) 
        next(err);
    } 
  }  
}